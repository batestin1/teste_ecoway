provider "aws" {
  region = var.region
}

module "ec2" {
  source = "./module/ec2"
}

module "rds" {
  source = "./module/rds"
}

module "s3" {
  source = "./module/s3"
}

module "vpc" {
  source = "./module/vpc"
}
