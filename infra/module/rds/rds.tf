module "bucket" {
  source = "../s3"
}

resource "aws_db_instance" "db_ecoway" {
  allocated_storage = var.allocated_storage
  storage_type = var.storage_type
  engine = var.engine
  engine_version = var.engine_version
  instance_class = var.instance_class
  db_name = var.name
  username = var.username
  password = var.password
  port = var.port
  identifier = var.identifier
  parameter_group_name = var.parameter_group_name
  skip_final_snapshot = var.skip_final_snapshot

  provisioner "local-exec" { 
    command = "mysql -h ${aws_db_instance.db_ecoway.endpoint} -u ${var.username} -p${var.password} ${var.name} < ${path.module}/../s3/${module.bucket.bucket_name}/create_tables_ecoway.sql"
  }
}
