resource "aws_s3_bucket" "ecoway_main_frontend" {
  bucket = "ecoway-main-frontend"
  tags = {
    Name    = "ecoway-main-frontend"
    Projeto = "Digital House"
  }
}

resource "aws_s3_bucket" "ecoway_main_backend" {
  bucket = "ecoway-main-backend"
  tags = {
    Name    = "ecoway-main-backend"
    Projeto = "Digital House"
  }
}

resource "aws_s3_bucket" "ecoway_main_banco_dados" {
  bucket = "ecoway-main-banco-dados"
  tags = {
    Name    = "ecoway-main-banco-dados"
    Projeto = "Digital House"
  }
}

resource "aws_s3_object" "ecoway_main_frontend" {
  for_each = fileset("${path.module}/../../../frontend", "**/*")
  bucket = aws_s3_bucket.ecoway_main_frontend.id
  key    = each.value
  source = "${path.module}/../../../frontend/${each.value}"
}

resource "aws_s3_object" "ecoway_main_backend" {
  for_each = fileset("${path.module}/../../../backend", "**/*")
  bucket = aws_s3_bucket.ecoway_main_backend.id
  key    = each.value
  source = "${path.module}/../../../backend/${each.value}"
}

resource "aws_s3_object" "ecoway_main_banco_dados" {
  for_each = fileset("${path.module}/../../../data", "**/*")
  bucket = aws_s3_bucket.ecoway_main_banco_dados.id
  key    = each.value
  source = "${path.module}/../../../data/${each.value}"
}

output "frontend_bucket_name" {
  value = aws_s3_bucket.ecoway_main_frontend.id
}

output "backend_bucket_name" {
  value = aws_s3_bucket.ecoway_main_backend.id
}

output "banco_dados_bucket_name" {
  value = aws_s3_bucket.ecoway_main_banco_dados.id
}

