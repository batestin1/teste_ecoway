variable "aws_ami_front" {
  type        = string
  description = "armazena a ami da EC2"
  default     = "ami-05b5badc2f7ddd88d" #valor da variavel
}

variable "aws_ami_back" {
  type        = string
  description = "armazena a ami da EC2"
  default     = "ami-007868005aea67c54" #valor da variavel
}

variable "aws_type_micro" {
  type        = string
  description = "armazena o tipo de micro que vamos usar"
  default     = "t2.micro" #valor da variavel
}

variable "instance_tags" {
  type        = map(string)
  description = "value"
  default = {
    Name    = "Ubuntu"
    Project = "Terraform"
  }
}