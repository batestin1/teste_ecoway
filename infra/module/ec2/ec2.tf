module "vpc" {
  source = "../vpc"
}

module "bucket" {
  source = "../s3"
}

resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  key_name   = "myKey"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "local_file" "ssh_key" {
  filename = "${aws_key_pair.kp.key_name}.pem"
  content  = tls_private_key.pk.private_key_pem
}

resource "aws_instance" "front-end" {
  ami           = var.aws_ami_front
  instance_type = var.aws_type_micro
  subnet_id     = module.vpc.subnet_id
  key_name      = aws_key_pair.kp.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [
    module.vpc.security_group_id
  ]
  user_data = <<-EOF
    #!/bin/bash
    sudo yum update -y
    sudo yum install -y nodejs
    sudo yum install -y npm
    sudo yum install -y git
    sudo yum install apache2
    sudo ufw app list
    sudo ufw allow 'Apache'
    sudo systemctl start apache2
    sudo yum install maven
    gvm install springboot
    aws s3 cp s3://${module.bucket.frontend_bucket_name} /path/to/local/directory
  EOF
}

resource "aws_instance" "back-end" {
  ami           = var.aws_ami_back
  instance_type = var.aws_type_micro
  subnet_id     = module.vpc.subnet_id
  key_name      = aws_key_pair.kp.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [
    module.vpc.security_group_id
  ]
  user_data = <<-EOF
    #!/bin/bash
    sudo yum update -y
    sudo yum install -y java-1.8.0-openjdk
    sudo yum install -y git
    sudo yum install apache2
    sudo ufw app list
    sudo ufw allow 'Apache'
    sudo systemctl start apache2
    aws s3 cp s3://${module.bucket.backend_bucket_name} /path/to/local/directory
  EOF
}

resource "aws_eip" "front-end" {
  vpc      = true
  instance = aws_instance.front-end.id
}

resource "aws_eip" "back-end" {
  vpc      = true
  instance = aws_instance.back-end.id
}

resource "aws_eip_association" "front-end" {
  instance_id   = aws_instance.front-end.id
  allocation_id = aws_eip.front-end.id

  depends_on = [
    aws_instance.front-end,
    aws_eip.front-end,
  ]
}

resource "aws_eip_association" "back-end" {
  instance_id   = aws_instance.back-end.id
  allocation_id = aws_eip.back-end.id

  depends_on = [
    aws_instance.back-end,
    aws_eip.back-end,
  ]
}
