# Arquitetura do projeto

Este projeto possui a seguinte arquitetura:



Descrição:

1. `backend`: Esta pasta contém todo o código-fonte do backend do projeto, escrito em Java. Há dois níveis de subpastas: `pasta1` e `pasta2`, onde cada uma contém arquivos Java relacionados a diferentes partes do backend.

2. `data`: Esta pasta contém um único arquivo SQL chamado `create_tables_ecoway.sql`, que é usado para criar as tabelas do banco de dados.

3. `frontend`: Esta pasta contém todo o código-fonte do frontend do projeto, escrito em JavaScript. Há dois níveis de subpastas: `pasta1` e `pasta2`, onde cada uma contém arquivos JavaScript relacionados a diferentes partes do frontend.

4. `infra`: Esta pasta contém todo o código de infraestrutura do projeto. O arquivo `main.tf` é o arquivo principal do Terraform e descreve a infraestrutura completa a ser criada. A pasta `module` contém módulos do Terraform para diferentes partes da infraestrutura, como instâncias EC2, instâncias RDS, buckets S3, VPCs e políticas IAM. O arquivo `variables.tf` define todas as variáveis de entrada para o código do Terraform.

Conclusão:

Essa arquitetura permite a separação clara do código de backend e frontend, além de separar o código de infraestrutura em um diretório separado. Isso permite que as partes do projeto sejam gerenciadas e mantidas separadamente, facilitando a escalabilidade do projeto.

## Como subir a infraestrutura para a AWS

Para subir a infraestrutura descrita no arquivo `main.tf` para a AWS, é necessário configurar o AWS CLI no seu terminal e executar os seguintes comandos:

1. Configurar o AWS CLI com suas credenciais:

aws configure

2. Instalar o Terraform:

sudo apt-get install -y unzip
wget https://releases.hashicorp.com/terraform/1.1.5/terraform_1.1.5_linux_amd64.zip
unzip terraform_1.1.5_linux_amd64.zip
sudo mv terraform /usr/local/bin/


3. Inicialize o Terraform:

terraform init


4. Verifique se não há erros na configuração do Terraform:

terraform validate


5. Visualize o plano de execução do Terraform:

terraform plan


6. Aplique as mudanças definidas no arquivo `main.tf`:

terraform apply


7. Confirme a aplicação das mudanças digitando `yes` no prompt que aparecer.

8. Verifique se a infraestrutura foi criada com sucesso na AWS.

9. Em sua pasta de infra de o seguinte comando:

    ssh-keygen
    e depois
    sudo chmod -R 777 * 
